package de.baddomain.TDCNB.apiCalls;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class twitchDataFetcher {
    public String user_name;
    public String title;
    public String user_login;

    public twitchDataFetcher(String broadcaster_id, String client_id, String client_secret){

        try {
            String url = "https://api.twitch.tv/helix/channels?broadcaster_id=" + broadcaster_id;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            // optional default is GET
            con.setRequestProperty("Authorization", "Bearer " + getToken(client_id, client_secret));
            con.setRequestProperty("Client-Id", client_id);
            con.setRequestMethod("GET");
            //add request header
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //extract data json object--------------------------------------------
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(response.toString());
            JsonNode dataNode = jsonNode.get("data");
            //--------------------------------------------------------------------

            //fetch data from json array------------------------------------------
            JsonNode array = mapper.readValue(dataNode.toString(), JsonNode.class);
            JsonNode object = array.get(0);
            System.out.println(object);
            user_name = object.get("broadcaster_name").textValue();
            title = object.get("title").textValue();
            user_login = object.get("broadcaster_login").textValue();
            //--------------------------------------------------------------------
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getToken(String id, String secret){
        try {
            String url = "https://id.twitch.tv/oauth2/token?client_id=" + id + "&client_secret=" + secret + "&grant_type=client_credentials";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            // optional default is GET
            con.setRequestMethod("POST");
            //add request header
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            //Read JSON response and print

            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(response.toString());
            JsonNode tokenNode = jsonNode.get("access_token");
            System.out.println("Successfully requested token.");
            return(tokenNode.textValue());

        }catch (Exception e){
            e.printStackTrace();
            return "error";
        }
    }
}
