package de.baddomain.TDCNB.webhooks;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.baddomain.TDCNB.apiCalls.twitchDataFetcher;
import de.baddomain.TDCNB.util.settingsManager;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.permission.Permissions;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.io.IOException;

@RestController
public class webhookListener {

    @RequestMapping(
            method = RequestMethod.POST,
            path = "/webhooks/streamOnline"
    )

    public String streamOnline(@RequestBody String body){
        settingsManager sm = new settingsManager();
        System.out.println(body);

        try {
            sm.readSettings();
        } catch (IOException e) {
            e.printStackTrace();
            sm.setup();
            System.out.println("Please edit NotifyBot.properties and restart the server to finalize the setup!");
        }
        DiscordApi api = new DiscordApiBuilder()         //|
                .setToken(sm.rp.getProperty("bot_token"))//Connect to discord api
                .login().join();                         //|

        System.out.println("Bot invite link: " + api.createBotInvite(Permissions.fromBitmask(8)));

        System.out.println("Detected twitch web request!");

        //try to read challenge object (used for validation)----------------------
        try{
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(body.toString());
            JsonNode dataNode = jsonNode.get("challenge");
            System.out.println("Challenge detected:" + dataNode.textValue());
            return dataNode.textValue();

        //when read unsuccessful try to extract stream data
        }catch(Exception e){
            try {
                System.out.println("Attempting to extract stream data from new Api call.");

                //get data from new api call------------------------------------------
                twitchDataFetcher fetcher = new twitchDataFetcher(sm.rp.getProperty("streamer_id"), sm.rp.getProperty("user_id"), sm.rp.getProperty("user_key"));
                System.out.println(fetcher.title);
                //--------------------------------------------------------------------

                //post in discord-----------------------------------------------------
                    Object[] channels = api.getTextChannelsByName(sm.rp.getProperty("dc_channel")).toArray();
                    TextChannel channel = (TextChannel) channels[0];
                    System.out.println("Detected channel: " + channel);
                    EmbedBuilder embed = new EmbedBuilder()
                            .setAuthor("Announcement Bot", "", sm.rp.getProperty("bot_icon_url"))
                            .addField( fetcher.user_name + " started streaming!", fetcher.title)
                            .setColor(new Color(100, 65, 164))
                            .setImage("https://static-cdn.jtvnw.net/previews-ttv/live_user_" + fetcher.user_login + "-320x180.jpg");
                    channel.sendMessage(embed);
                    System.out.println("Stream went online!");
                //--------------------------------------------------------------------
                return "success, 200";
            }catch (Exception exception){
                return "success, 200";
            }

        }

    }

}